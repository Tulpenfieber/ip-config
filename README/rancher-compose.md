# Rancher-compose helper image

This image helps you running a rancher-compose deployment in a pipeline.\
The base image is [Alpine](https://hub.docker.com/_/alpine).\
Inside that image [rancher-compose](https://github.com/rancher/rancher-compose/releases) lives in /usr/local/bin.\
There is a small helper script (update-rancher-compose.sh) which does the image creation and the push to a docker registry.

## Prerequisites
* [Docker](https://www.docker.com/)
* [rancher-compose](https://github.com/rancher/rancher-compose/releases)

## Steps for updating the rancher-compose container
1. Change RANCHER_COMPOSE_VERSION in .env file to the version you downloaded to rancher-compose-binary folder. The new image will get that version
2. Change ALPINE_VERSION in .env file to the Apline version you like to use. This will be the base image version
3. Update and push your new image by simply typing: `./update-rancher-compose.sh`
