# Pipeline readme for production
Pipeline stages for prod are:
* build
* dockerize
* deploy

They only run if there is a merge request or a push to the main branch.\
* Stage build
  * Makes use of a Maven container to generate Jar files. Those files are put into a gitlab cache to take them to next stage
* Stage dockerize
  * A small check if docker is available by just showing the available docker contexts
  * Next step is displaying the docker-compose config to check if all variables are proper
  * Takes the Jar file from cache and uses docker-compose with profile `prod-image-build`
  * Login to docker registry to finally push the image
* Stage deploy
  * Deploy the image with rancher-compose to target Rancher

## Prerequisites
* [Docker](https://www.docker.com/)

There are some Gitlab variables to set:
* GITLAB_TOKEN_NAME: To access container registry on registry.gitlab.com
* GITLAB_TOKEN_VALUE: To access container registry on registry.gitlab.com
* IP_CONFIG_VERSION: Version of the created ip-config image > Raise the version number before pushing to main branch
* JAVA_VERSION: Version of the base image for creating ip-config image
* MAVEN_VERSION: Maven version you like to use
* RANCHER_COMPOSE_VERSION: The version of rancher-compose. This is used to pull the image created by yourself
* RANCHER_URL: Used by rancher-compose image and deploy stage to access target Rancher instance
* RANCHER_ACCESS_KEY: Used to authenticate against target Rancher instance
* RANCHER_SECRET_KEY: Used to authenticate against target Rancher instance

## Used docker-compose profiles
+ To create Jar files (located in Gitlab cache) with Maven container:`local-maven-package`
* To create a finalized image based on Bitnami Java: `prod-image-build`
