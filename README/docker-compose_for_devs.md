# Docker-compose readme for developers

You don't need any installed packages on your local system. Everything during development runs in a container with [Maven](https://hub.docker.com/_/maven) image.\
When everything is ready from dev side the [Java from Bitnami](https://hub.docker.com/r/bitnami/java) image is used as base to create another image for production or local use.\
Read below about docker-compose profiles to know which profile to use.\
Set the Maven, Java and the ip-config versions in `.env` file\
The .env file is only used in local development!\
To circumvent Maven downloading during every run the repository files, there is a `maven-repo` folder. This is a volume mount for the Maven container

IGNORE PROFILES MARKED AS PROD > THEY ARE USED IN PIPELINE!\

ALWAYS WORK ON BRANCHES THE PIPELINES WILL THANK YOU ;)

## Prerequisites
* [Docker](https://www.docker.com/)

## Docker-compose profiles
### Development
+ To run project local (./target folder will appear) with Maven container: `local-maven-run`
+ To verify project local (./target folder will appear) with Maven container: `local-maven-verify`
+ To create local (./target folder will appear) Jar files with Maven container:`local-maven-package`
+ To run unit test for IpConfigControllerIT-getIpConfigs: `local-maven-test`
### Build
* To create a finalized image based on Bitnami Java: `local-image-build`
### Run
+ To run finalized image local: `local-image-run`

## Local steps for creating container
1. Create Jar file: `docker-compose --profile local-maven-package up`
2. Build finalized image: `docker-compose --profile local-image-build build`
3. Start container with finalized image: `docker-compose --profile local-image-run up -d`
4. Check browser on [this](http://localhost:8080/ip-configs) page
5. Delete all used containers/networks: `docker-compose down`
6. Cleanup Maven output folder `rm -rf target/`
7. Cleanup Maven download folder `cd maven-repo && rm -rf !(placeholder.txt) && cd ..`

## Debugging with Maven
* Create a not detached Maven container which runs your Jar file: `docker-compose --profile local-maven-run up`
* Run a not detached verify session with your Jar file in Maven: `docker-compose --profile local-maven-verify up`
