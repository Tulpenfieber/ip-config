# Pipeline readme for development
Pipeline stages for developemt are:
* test-IpConfigControllerIT-getIpConfigs

They always run with a push to a branch, a merge request to master and a push to master .\
* Stage test-IpConfigControllerIT-getIpConfigs
    * Got to Gitlab > your project > CI/CD > Pipelines > selct your pipeline > click on "Tests" tab > you see a summary
    * To download created XML file, go to Gitlab > your project > CI/CD > Artifacts > select job and download files

## Prerequisites
* [Docker](https://www.docker.com/)

There are some Gitlab variables to set:
* MAVEN_VERSION: Maven version you like to use

## Used docker-compose profiles
+ To run unit test for IpConfigControllerIT-getIpConfigs: `local-maven-test`
