ARG JAVA_VERSION
FROM bitnami/java:${JAVA_VERSION}
COPY ["target/ip-config-0.0.1-SNAPSHOT.jar", "/app/ip-config-0.0.1-SNAPSHOT.jar"]
CMD java -jar /app/ip-config-0.0.1-SNAPSHOT.jar